﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Overriding_Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            Base temp1 = new Base();
            Base2 temp2 = new Base2();

            temp1.hello();
            temp1.hello2();
            temp2.hello();
            temp2.hello2();
        }
    }

    class Base
    {
        //this method cannot be overriden by any derived classes
        //as I have not made it virtual
        public void hello()
        {
            Debug.WriteLine("Base - hello - This is me saying hello from base class");
        }

        //this method is virtual, allowing derived classes to override it
        public virtual void hello2()
        {
            Debug.WriteLine("Base - hello - This is me saying hello2 from base class");
        }
    }

    class Base2 : Base
    {

        //I cannot do the following becuase of the non-virtual status of hello in the
        //base class
        //public override void hello()

        //I can do the following
        public override void hello2()
        {
            Debug.WriteLine("Base2 - hello2 - This is me saying hello2 from derived class");
        }
    }
}
